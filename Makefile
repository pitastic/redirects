####
#
# makefile for cloudformation stacks for the pitastic redirects
#
####

#
# VARIABLES
#

# name of the cloudformation stack to create
STACK_NAME?=pitastic-redirects

# general shortcuts
DOCKER=docker run --rm -ti --env-file ${CURDIR}/.env -v ${CURDIR}:/data -w /data
BASH=${DOCKER} --entrypoint=/bin/bash piaws -c
JQ=${DOCKER} --entrypoint=/usr/bin/jq piaws

#
# TARGETS
#

# make all
all: docker-image cloudformation-cert cloudformation-redirects

# build docker image
docker-image:
	docker build -t piaws .

# deploy cloudformation stack for the cloudfront certificate
cloudformation-cert:
	${BASH} "aws --region=us-east-1 cloudformation deploy \
		--stack-name ${STACK_NAME}-cert --template-file Cloudformation.certificate.yaml \
		--no-fail-on-empty-changeset"

# deploy cloudfromation stack for the s3 and cloudfront infrastructure
cloudformation-redirects:
	# first we get read out the info about the certificate stack
	${BASH} "aws --region=us-east-1 cloudformation describe-stacks --stack-name ${STACK_NAME}-cert > .certificate.stack"
	# now we read out the arn from the certificate stack and write it down
	${JQ} -r '.Stacks[0].Outputs[] | select (.OutputKey == "Certificate") | .OutputValue' .certificate.stack > .certificate.arn
	# next step is to run the deployment for the redirects
	${BASH} "aws cloudformation deploy \
		--stack-name ${STACK_NAME} --template-file Cloudformation.redirects.yaml \
		--no-fail-on-empty-changeset \
		--parameter-overrides Certificate=$$(cat .certificate.arn)"
